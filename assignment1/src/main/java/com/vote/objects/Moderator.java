package com.vote.objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;


public class Moderator {
	@Null
	public Integer id;
	@NotNull
	public String name;
	@NotNull
	public String email;
	@NotNull
	public String password;
	@Null
	public String created_at;
	

}
