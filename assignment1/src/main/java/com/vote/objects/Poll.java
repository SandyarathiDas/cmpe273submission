package com.vote.objects;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonView;

public class Poll {
	public String id;
	@NotNull
	public String question;
	@NotNull
	public String started_at;
	@NotNull
	public String expired_at;
	@NotNull
	public String[] choice;
	public int[] results;
	
	
}
